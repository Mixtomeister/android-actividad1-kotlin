package com.thinkback.actividad1_kotlin.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.thinkback.actividad1_kotlin.R
import com.thinkback.actividad1_kotlin.model.*

class BookActivity : AppCompatActivity(), EventManagerListener {

    private lateinit var events: EventManager
    private lateinit var btn_perfil: Button
    private lateinit var btn_ant: Button
    private lateinit var btn_sig: Button
    private lateinit var txt_titulo: EditText
    private lateinit var txt_contenido: EditText
    private lateinit var libro: Libro

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book)

        this.libro = DataManager.libro
        this.events = EventManager(this)

        btn_perfil = findViewById(R.id.btn_perfil)
        btn_ant = findViewById(R.id.btn_ant)
        btn_sig = findViewById(R.id.btn_sig)
        txt_titulo = findViewById(R.id.txt_titulo)
        txt_contenido = findViewById(R.id.txt_contenido)

        btn_perfil.setOnClickListener(events)
        btn_ant.setOnClickListener(events)
        btn_sig.setOnClickListener(events)

        llenarLibro()
        cargarPagina()
    }

    private fun llenarLibro(){
        this.libro.addPagina(Pagina(getString(R.string.tit_cap1), getString(R.string.con_cap1)))
        this.libro.addPagina(Pagina(getString(R.string.tit_cap2), getString(R.string.con_cap2)))
        this.libro.addPagina(Pagina(getString(R.string.tit_cap3), getString(R.string.con_cap3)))
        this.libro.addPagina(Pagina(getString(R.string.tit_cap4), getString(R.string.con_cap4)))
    }

    fun cargarPagina() {
        val pag = this.libro.getActPag()
        if (this.libro.first) {
            btn_ant.isEnabled = false
            btn_sig.isEnabled = true
        } else if (this.libro.last) {
            btn_ant.isEnabled = true
            btn_sig.isEnabled = false
        } else {
            btn_ant.isEnabled = true
            btn_sig.isEnabled = true
        }
        txt_titulo.setText(pag.titulo)
        txt_contenido.setText(pag.contenido)
    }

    override fun onClick(view: View?) {
        if (view?.id == R.id.btn_perfil) {
            finish()
        } else if (view?.id == R.id.btn_ant) {
            this.libro.pag--
            cargarPagina()
        } else if (view?.id == R.id.btn_sig) {
            this.libro.pag++
            cargarPagina()
        }
    }
}
