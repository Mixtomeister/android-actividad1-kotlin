package com.thinkback.actividad1_kotlin.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import com.thinkback.actividad1_kotlin.R
import com.thinkback.actividad1_kotlin.model.DataManager
import com.thinkback.actividad1_kotlin.model.EventManager
import com.thinkback.actividad1_kotlin.model.EventManagerListener
import com.thinkback.actividad1_kotlin.model.Perfil

class MainActivity : AppCompatActivity(), EventManagerListener {

    private lateinit var btn_edit_save: Button
    private lateinit var btn_sig_can: Button
    private lateinit var txt_name: EditText
    private lateinit var txt_email: EditText
    private lateinit var txt_tlf: EditText
    private lateinit var txt_dir: EditText
    private lateinit var dt_fecha: DatePicker
    private var editable: Boolean = false
    private lateinit var events: EventManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.events = EventManager(this)

        this.btn_edit_save = findViewById(R.id.btn_edit_save)
        this.btn_sig_can = findViewById(R.id.btn_sig_can)
        this.txt_name = findViewById(R.id.txt_name)
        this.txt_email = findViewById(R.id.txt_email)
        this.txt_tlf = findViewById(R.id.txt_tlf)
        this.txt_dir = findViewById(R.id.txt_dir)
        this.dt_fecha = findViewById(R.id.dt_fecha)

        this.btn_edit_save.setOnClickListener(events)
        this.btn_sig_can.setOnClickListener(events)

        cargarDatos()
        setNotEditable()
    }

    fun setNotEditable() {
        this.editable = false
        btn_sig_can.setText(R.string.btn_book)
        btn_edit_save.setText(R.string.btn_edit)
        txt_name.isEnabled = false
        txt_email.isEnabled = false
        txt_tlf.isEnabled = false
        txt_dir.isEnabled = false
        dt_fecha.isEnabled = false
        txt_name.isFocusable = false
        txt_email.isFocusable = false
        txt_tlf.isFocusable = false
        txt_dir.isFocusable = false
        txt_name.isFocusableInTouchMode = false
        txt_email.isFocusableInTouchMode = false
        txt_tlf.isFocusableInTouchMode = false
        txt_dir.isFocusableInTouchMode = false
    }

    fun setEditable() {
        editable = true
        btn_sig_can.setText(R.string.btn_can)
        btn_edit_save.setText(R.string.btn_save)
        txt_name.isEnabled = true
        txt_email.isEnabled = true
        txt_tlf.isEnabled = true
        txt_dir.isEnabled = true
        dt_fecha.isEnabled = true
        txt_name.isFocusable = true
        txt_email.isFocusable = true
        txt_tlf.isFocusable = true
        txt_dir.isFocusable = true
        txt_name.isFocusableInTouchMode = true
        txt_email.isFocusableInTouchMode = true
        txt_tlf.isFocusableInTouchMode = true
        txt_dir.isFocusableInTouchMode = true
    }

    fun guardarDatos() {
        val perfil: Perfil = DataManager.perfil
        perfil.nombre = txt_name.text.toString()
        perfil.email = txt_email.text.toString()
        perfil.tlf = txt_tlf.text.toString()
        perfil.direccion = txt_dir.text.toString()
        perfil.dia = dt_fecha.dayOfMonth
        perfil.mes = dt_fecha.month
        perfil.year = dt_fecha.year
    }

    fun cargarDatos() {
        val perfil: Perfil = DataManager.perfil
        txt_name.setText(perfil.nombre)
        txt_email.setText(perfil.email)
        txt_tlf.setText(perfil.tlf)
        txt_dir.setText(perfil.direccion)
        if (perfil.dia != -1)
            dt_fecha.updateDate(perfil.year, perfil.mes, perfil.dia)
    }

    fun isEditable(): Boolean = this.editable

    override fun onClick(view: View?) {
        if (view?.id == R.id.btn_edit_save) {
            if (isEditable()) {
                setNotEditable()
                guardarDatos()
            } else {
                setEditable()
            }
        } else if (view?.id == R.id.btn_sig_can) {
            if (isEditable()) {
                setNotEditable()
            } else {
                val intent = Intent(this, BookActivity::class.java)
                startActivity(intent)
            }
        }
    }
}
