package com.thinkback.actividad1_kotlin.model

import android.util.Log
import com.thinkback.actividad1_kotlin.R

object DataManager{
    var perfil: Perfil
    var libro: Libro

    init {
        this.perfil = Perfil()
        this.libro = Libro()
    }
}
