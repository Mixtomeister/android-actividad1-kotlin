package com.thinkback.actividad1_kotlin.model

import android.view.View

class EventManager: View.OnClickListener{
    var activity: EventManagerListener

    constructor(listener: EventManagerListener){
        this.activity = listener
    }

    override fun onClick(v: View?) {
        this.activity.onClick(v)
    }
}

interface EventManagerListener{
    fun onClick(view: View?)
}

