package com.thinkback.actividad1_kotlin.model

import java.util.ArrayList

class Libro {
    private var paginas: ArrayList<Pagina>
    var pag: Int
    var first: Boolean
    var last: Boolean

    init {
        this.paginas = ArrayList()
        this.pag = 0
        this.first = false
        this.last = false
    }

    fun getActPag(): Pagina{
        if(this.pag == 0) {
            this.first = true
        } else if (this.pag == this.paginas.size - 1) {
            this.last = true
        } else {
            this.first = false
            this.last = false
        }
        return this.paginas[this.pag]
    }

    fun addPagina(pag: Pagina) {
        this.paginas.add(pag)
    }
}

class Pagina(var titulo: String, var contenido: String)
