package com.thinkback.actividad1_kotlin.model

class Perfil {
    var nombre: String? = null
    var email: String? = null
    var tlf: String? = null
    var direccion: String? = null
    var dia: Int = 0
    var mes: Int = 0
    var year: Int = 0

    init {
        this.nombre = ""
        this.email = ""
        this.tlf = ""
        this.direccion = ""
        this.dia = -1
        this.mes = -1
        this.year = -1
    }
}